package haxeris.controller;

import js.html.KeyboardEvent;
import haxeris.data.Game;

abstract GameController(Game) {

    public function new(game:Game) {
        this = game;
        js.Browser.document.addEventListener( "keydown", onKeyDown );
    }

    private function onKeyDown(e:KeyboardEvent)
    {
        if(Type.enumEq(this.gameState, RUNING)){
            switch(e.keyCode) {
                case KeyboardEvent.DOM_VK_UP: this.tryRotatePlayerBlock(RIGHT);
                case KeyboardEvent.DOM_VK_LEFT: this.tryMovePlayerBlock(LEFT);
                case KeyboardEvent.DOM_VK_RIGHT: this.tryMovePlayerBlock(RIGHT);
                case KeyboardEvent.DOM_VK_DOWN: this.tryMovePlayerBlock(DOWN);
            }
        }

        switch(e.keyCode) {
            case KeyboardEvent.DOM_VK_SPACE: onSpacePressed();
        }
    }

    private inline function onSpacePressed(){
        js.Browser.document.activeElement.blur();
        changeGameState();
    }

    private inline function changeGameState(){
        switch this.gameState{
            case PAUSED: this.run();
            case RUNING: this.pause();
            case GAME_OVER: this.newGame();
        }
    }
}
