package haxeris.ui;

import coconut.ui.View;
import haxeris.data.Game;

@:less("hud.less")
class HUDView extends View{

    private static inline var DEFAULT_GAME_SPEED:Int = 50;

    @:attribute function newGame():Void;
    @:attribute function run():Void;
    @:attribute function pause():Void;
    @:attribute var score:Int;

    @:attribute var onGameSpeedChanged:Int->Void;
    @:state var gameSpeed:Int = DEFAULT_GAME_SPEED;

    function render () return @hxx '
	<div class="hud">
		<h2 class="score">Score: ${score}</h2>
	    <div class="buttons">
	    	<button class="btn new-game" onclick=${newGame()}>New Game</button>
	        <button class="btn start" onclick=${run()}>Start</button>
	        <button class="btn pause" onclick=${pause()}>Pause</button>
	    </div>
	    <div class="game-speed">
	        <span>Game Speed:</span>
            <input type="range" min="1" max="100" value=${''+gameSpeed} class="slider" onchange=${setGameSpeed(event.src.value)}/>
            <span>${gameSpeed}</span>
            <button class="btn reset-speed" onclick=${resetSpeed()}>Reset</button>
        </div>
	</div>';

    private function setGameSpeed(val:String){
        gameSpeed = Std.parseInt(val);
        onGameSpeedChanged(gameSpeed);
    }

    private function resetSpeed(){
        gameSpeed = DEFAULT_GAME_SPEED;
    }
}
