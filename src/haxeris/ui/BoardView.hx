package haxeris.ui;

import haxeris.core.BoardCellValue;
import haxeris.core.factory.ShapedBlockFactory.ShapedBlockValue;
import haxeris.core.Board.BoardData;
import haxeris.core.Board;
import coconut.ui.View;

@:less("board.less")
class BoardView extends View{

    @:attribute var gameBoard:Board<ShapedBlockValue>;

    function render ()
    {
        var boardData:BoardData<ShapedBlockValue> = gameBoard;

        return @hxx '
		<table className="board">
			<tbody>
				<for {row in boardData}>
					<tr>
						<for {cell in row}>
						    <let cellValue=${cell.value}>
						        <td class="cell ${getCellName(cellValue)} ${getActiveClass(cellValue)}"> </td>
						    </let>
						</for>
					</tr>
				</for>
			</tbody>
		</table>';
    }

    private inline function getCellName(val:BoardCellValue<ShapedBlockValue>):String{
        return val.isEmpty() ? 'empty' : 'shape-${val.getValue().shape}';
    }

    private inline function getActiveClass(val:BoardCellValue<ShapedBlockValue>):String{
        return val.isActive() ? 'player' : '';
    }

}
