package haxeris.ui;

import haxeris.data.Game;
import coconut.ui.View;

@:less("game.less")
class GameView extends View{
    @:attribute var game:Game;

    function render () return @hxx '
	<div class="game">
	    <header>
	       <h1 class="game-state ${getGameStateClass(game.gameState)}">${getGameStateStr(game.gameState)}</h1>
	    </header>
	    <div class="container">
	        <BoardView gameBoard=${game.gameBoard} />
	        <HUDView newGame=${game.newGame}
	                 run=${game.run}
	                 pause=${game.pause}
	                 score=${game.score}
	                 onGameSpeedChanged=${setGameSpeed}/>
	    </div>
	</div>';

    private function getGameStateStr(state:GameState){
        return switch state{
            case GameState.GAME_OVER: 'Game Over';
            case GameState.RUNING: 'Runing';
            case GameState.PAUSED: 'Paused';
        }
    }

    private function getGameStateClass(state:GameState){
        return switch state{
            case GameState.GAME_OVER: 'game-over';
            case GameState.RUNING: 'runing';
            case GameState.PAUSED: 'pause';
        }
    }

    private function setGameSpeed(val:Int){
        if(val <= 0){
            val = 1;
        }

        var tickTime = 600 - Std.int(val/100 * 600);
        trace(tickTime);
        game.setTickTime(tickTime);
    }
}
