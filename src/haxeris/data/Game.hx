package haxeris.data;

import tink.state.State;
import haxeris.core.factory.ShapedBlockFactory.ShapedBlockValue;
import haxeris.core.Block;
import haxeris.core.Board;
import coconut.data.Model;

class Game implements Model{

    @:computed var width:Int = board.columns;
    @:computed var height:Int = board.rows;
    @:computed var gameBoard:Board<ShapedBlockValue> = createGameBoard(board, playerBlock);
    @:computed var tickTime:Int = gameTimer.time;

    @:observable var gameState:GameState = @byDefault PAUSED;
    @:observable var score:Int = @byDefault 0;


    @:observable private var board:Board<ShapedBlockValue>;
    @:observable private var playerBlock:Block<ShapedBlockValue> = @byDefault null;

    @:constant private var blockFactory:{ function getBlock():Block<ShapedBlockValue>; };
    @:skipCheck @:constant private var gameTimer:GameTimer;

    public function new(){
        gameTimer.tick = step;
        this.observables.gameState.bind(value->{
            switch value{
                case PAUSED: gameTimer.stop();
                case RUNING: gameTimer.start();
                case GAME_OVER: gameTimer.stop();
            }
        });
    }

    static public function create(width:Int, height:Int, getBlock:Void->Block<ShapedBlockValue>, tickTime:Int = 300){
        return new Game({
            board: new Board(width, height),
            blockFactory: {getBlock: getBlock},
            gameTimer: new GameTimer(tickTime)
        });
    }

    public function newGame(){
        pause();
        this.board.clear();
        tryGetNewPlayerBlock();
    }

    @:transition public function run(){
        return {gameState: RUNING}
    }

    @:transition public function pause(){
        return {gameState: PAUSED};
    }

    @:transition public function gameOver(){
        return {gameState: GAME_OVER}
    }

    public function step(){
        if(canMoveDown()){
            tryMovePlayerBlock(DOWN);
        }else{
            lockPlayerBlockToBoard();
            handleFullRows();
            tryGetNewPlayerBlock();
        }
    }

    public function tryRotatePlayerBlock(dir:RotDir){
        var block = playerBlock.clone();
        switch dir{
            case RotDir.LEFT: block.rotateLeft();
            case RotDir.RIGHT: block.rotateRight();
        }

        return tryPlaceBlock(block);
    }

    public function tryMovePlayerBlock(dir:MoveDir){
        var block = playerBlock.clone();
        switch dir{
            case MoveDir.LEFT: block.moveLeft();
            case MoveDir.RIGHT: block.moveRight();
            case MoveDir.DOWN: block.moveDown();
        }

        return tryPlaceBlock(block);
    }

    public function setTickTime(val:Int){
        gameTimer.time = val;
    }

    @:transition private function tryGetNewPlayerBlock(){
        var block = blockFactory.getBlock();
        var blockBounds = block.calculateBouns();
        block.moveTo(Std.int(width/2), -blockBounds.minY);

        if(canPlace(block)){
            return {playerBlock: block};
        }else{
            gameOver();
            return {};
        }
    }

    @:transition private function tryPlaceBlock(block:Block<ShapedBlockValue>){
        return canPlace(block) ? {playerBlock: block} : {};
    }

    @:transition private function lockPlayerBlockToBoard(){
        for(cell in playerBlock.getTranslatedCells())
            board.setAt(cell.val, cell.x, cell.y);
        return {board: board};
    }

    @:transition private function handleFullRows(){
        var workBoard = board.clone();
        var addScore = 0;
        for(y in 0...workBoard.rows){
            if(workBoard.isRowFull(y)){
                workBoard.removeRow(y);
                addScore += width;
            }
        }

        return {board:workBoard, score: score + addScore};
    }

    private inline function canPlace(block:Block<ShapedBlockValue>){
        return board.areEmpty(block.getCoordinates());
    }

    private static function createGameBoard(board:Board<ShapedBlockValue>, block:Block<ShapedBlockValue>){
        var boardClone = board.clone();
        if(block != null){
            for(cell in block.getTranslatedCells()){
                if(boardClone.isInside(cell.x, cell.y))
                    boardClone.setAt({val:cell.val, isActive:true}, cell.x, cell.y);
            }
        }
        return boardClone;
    }

    private function canMoveDown(){
        var block = playerBlock.clone();
        block.moveDown();

        return canPlace(block);
    }

}


enum GameState{
    RUNING;
    PAUSED;
    GAME_OVER;
}

enum RotDir{
    LEFT;
    RIGHT;
}

enum MoveDir{
    LEFT;
    RIGHT;
    DOWN;
}

private abstract GameTimer({timer:haxe.Timer, _tick:State<Void->Void>, _time:State<Int>, _runing:Bool}){
    public var time(get, set):Int;
    inline function get_time() return this._time.value;
    inline function set_time(val) {
        this._time.set(val);
        return val;
    }

    public var tick(get, set):Void->Void;
    inline function get_tick() return this._tick.value;
    inline function set_tick(val) {
        this._tick.set(val);
        return val;
    }

    public inline function new(time:Int = 0, tick:Void->Void = null){
        var ms = new State(time);
        ms.observe().bind((val)->restart());

        var tickState = new State(tick);
        tickState.observe().bind((val)->restart());

        this = {
            _time: ms,
            _tick: tickState,
            timer: null,
            _runing: false
        }
    }

    public inline function start(){
        if(this.timer != null) stop();

        this.timer = new haxe.Timer(this._time);
        this.timer.run = tick;
        this._runing = true;
    }

    public inline function stop(){
        if(this.timer != null){
            this.timer.stop();
        }
        this._runing = false;
    }

    public inline function restart(){
        if(this._runing) start();
    }
}




