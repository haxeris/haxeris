package haxeris.core;

import haxeris.core.common.Coordinate;
import tink.state.State;
import tink.pure.List;

typedef BoardCell<T> = State<BoardCellValue<T>>;
typedef BoardData<T> = List<List<BoardCell<T>>>;

abstract Board<T>(BoardData<T>) from BoardData<T> to BoardData<T>{

    public var rows(get, never):Int;
    inline function get_rows() {
        return this.length;
    }

    public var columns(get, never):Int;
    inline function get_columns() {
        return getRow(0).length;
    }

    public var numOfCells(get, never):Int;
    inline function get_numOfCells(){
        return rows * columns;
    }

    public inline function new(columns:Int, rows:Int){
        this = List.fromArray([for (x in 0...rows) List.fromArray([for (y in 0...columns) new BoardCell(BoardCellValue.EMPTY)])]);
    }


    public inline function getAt(x:Int, y:Int):BoardCell<T>{
        switch getRow(y).get(x){
            case None: throw 'Invalid col index: $x (max: $columns)';
            case Some(v): return v;
        }
    }

    public inline function getRow(y:Int){
        switch this.get(y){
            case None: throw 'Invalid row index: $y (max: $rows)';
            case Some(v): return v;
        }
    }

    public inline function setAt(value:BoardCellValue<T>, x:Int, y:Int){
        getAt(x,y).set(value);
    }

    public inline function forEach(iter:(val:BoardCell<T>, x:Int, y:Int)->Void){
        for(x in 0...columns)
            for(y in 0...rows)
                iter(getAt(x,y), x, y);
    }

    public inline function clearAt(x:Int, y:Int){
       setAt(BoardCellValue.EMPTY, x, y);
    }

    public function isRowFull(y:Int){
        for(x in 0...columns)
            if(getAt(x, y).value.isEmpty()){
                return false;
            }

        return true;
    }

    public inline function isRowEmpty(y:Int){
        for(x in 0...columns)
            if(!getAt(x, y).value.isEmpty()){
                return false;
            }

        return true;
    }

    public inline function isInside(x:Int, y:Int){
        return x >= 0 && y >= 0 && x < columns && y < rows;
    }

    public inline function isEmpty(x:Int, y:Int){
        return isInside(x, y) && getAt(x, y).value.isEmpty();
    }

    public function areEmpty(coords:Iterable<Coordinate>){
        for(coord in coords)
            if(!isEmpty(coord.x, coord.y)) return false;

        return true;
    }

    public inline function removeRow(y:Int){
        var arr = toArrayOfBoardCell();
        arr.splice(y, 1);
        arr.unshift([for (y in 0...columns) new BoardCell(BoardCellValue.EMPTY)]);
        forEach((cell, x, y)->cell.set(arr[y][x].value));
    }

    public inline function clearRow(y:Int){
        for(x in 0...columns)
            clearAt(x, y);
    }

    public inline function clear(){
        for(x in 0...columns)
            for(y in 0...rows)
                clearAt(x, y);
    }

    public inline function clone(){
        var cloned = new Board(columns, rows);
        forEach((cell, x, y)->cloned.setAt(cell.value, x, y));
        return cloned;
    }

    @:to
    public inline function toArrayOfBoardCell():Array<Array<BoardCell<T>>>{
        return clone().toArray().map(list->list.toArray());
    }

    private inline function toArray(){
        return this.toArray();
    }
}