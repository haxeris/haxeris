package haxeris.core.enums;

enum BlockShapeValue{
    Z;
    O;
    I;
    L;
    T;
}

abstract BlockShape(BlockShapeValue) from BlockShapeValue to BlockShapeValue {
    public inline function new(val) {
        this = val;
    }

    @:to
    public inline function toString(){
        return switch this{
            case Z:'Z';
            case O:'O';
            case I:'I';
            case L:'L';
            case T:'T';
        }
    }
}