package haxeris.core;

import tink.state.State;
import haxeris.core.common.Coordinate;

typedef BlockCellData<T> ={
    position:State<Coordinate>,
    val:State<T>
}

abstract BlockCell<T>(BlockCellData<T>){
    public var x(get, never):Int;
    inline function get_x() return this.position.value.x;

    public var y(get, never):Int;
    inline function get_y() return this.position.value.y;

    public var val(get, never):T;
    inline function get_val() return this.val.value;

    public inline function new(x:Int, y:Int, val:T){
        this = {
            position: new State({x:x, y:y}),
            val:new State(val)
        };
    }

    public inline function setPosition(x:Int, y:Int){
        this.position.set({x:x, y:y});
    }

    public function rotate(rx:Int, ry:Int){
        setPosition(y * rx, x * ry);
    }

    public function clone():BlockCell<T>{
        return new BlockCell<T>(x, y, val);
    }
}

