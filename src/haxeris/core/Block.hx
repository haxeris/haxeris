package haxeris.core;

import tink.state.State;
import tink.pure.List;
import haxeris.core.common.Coordinate;

using Lambda;

typedef Bounds = {minX:Int, minY:Int, maxX:Int, maxY:Int};

typedef BlockData<T> = {
    cells:List<BlockCell<T>>,
    position:State<Coordinate>
}

abstract Block<T>(State<BlockData<T>>){
    private var cells(get, never):List<BlockCell<T>>;
    inline function get_cells() return this.value.cells;

    public var position(get, never):State<Coordinate>;
    inline function get_position() return this.value.position;

    public var x(get, never):Int;
    inline function get_x() return position.value.x;

    public var y(get, never):Int;
    inline function get_y() return position.value.y;

    public inline function new(cells:List<BlockCell<T>>, x:Int = 0, y:Int = 0){
        this = new State({
            cells: cells,
            position: new State({x:x, y:y})
        });
    }

    public inline function clone():Block<T>{
        var clone = new Block<T>(
            cells.map(cell->return cell.clone()),
            x, y
        );

        return clone;
    }

    public inline function moveTo(x:Int, y:Int){
        position.set({x:x, y:y});
    }

    public inline function moveLeft(){
        moveTo(x -1, y);
    }

    public inline function moveRight(){
        moveTo(x +1, y);
    }

    public inline function moveDown(){
        moveTo(x, y+1);
    }

    private inline function rotate(rx:Int, ry:Int){
        for(cell in cells)
            cell.rotate(rx, ry);
    }

    public inline function rotateLeft(){
        rotate(1, -1);
    }

    public inline function rotateRight(){
        rotate(-1, 1);
    }

    public inline function getTranslatedCells():Iterable<BlockCell<T>>{
        return cells.map(cell->new BlockCell(cell.x + x, cell.y + y, cell.val));
    }

    public inline function getCoordinates():Iterable<Coordinate>{
        return cells.map(cell->{x:cell.x + x, y:cell.y + y});
    }

    @:from
    public static inline function fromArray<T>(array:Array<BlockCell<T>>):Block<T>{
        return new Block(List.fromArray(array));
    }

    public inline function calculateBouns():Bounds{
        return Lambda.fold(cells, (cell, bounds:Bounds)->{
            bounds.minX = Std.int(Math.min(cell.x, bounds.minX));
            bounds.minY = Std.int(Math.min(cell.y, bounds.minY));
            bounds.maxX = Std.int(Math.max(cell.x, bounds.maxX));
            bounds.maxY = Std.int(Math.max(cell.y, bounds.maxY));
            return bounds;
        },{
            minX: Std.int(Math.POSITIVE_INFINITY),
            minY: Std.int(Math.POSITIVE_INFINITY),
            maxX: Std.int(Math.NEGATIVE_INFINITY),
            maxY: Std.int(Math.NEGATIVE_INFINITY)
        });
    }

    public inline function toString(){
        var bounds = calculateBouns();
        var width = bounds.maxX - bounds.minX;
        var height = bounds.maxY - bounds.minY;
        var arr = [for (_ in 0...height+1) [for (_ in 0...width+1) '.']];

        for(cell in cells){
            arr[cell.y - bounds.minY][cell.x - bounds.minX] = 'x';
        }

        return arr.map(row->row.join('|')).join('\n');
    }
}
