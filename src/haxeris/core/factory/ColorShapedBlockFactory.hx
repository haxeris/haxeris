package haxeris.core.factory;

import haxeris.core.factory.ShapedBlockFactory.ShapedBlockValue;
import haxeris.core.common.Color;
import haxeris.core.enums.BlockShape;
import haxeris.core.factory.base.ShapedBlockFactoryBase;

typedef ColorBlockValue = { >ShapedBlockValue, color:Color};

class ColorShapedBlockFactory extends ShapedBlockFactoryBase<ColorBlockValue> {

    public function new() {
        super();
    }

    override public function getValueForShape(shape:BlockShape){
        return {shape:shape, color:switch shape{
            case BlockShapeValue.Z: Color.BLUE;
            case BlockShapeValue.O: Color.RED;
            case BlockShapeValue.I: Color.GREEN;
            case BlockShapeValue.L: Color.CYAN;
            case BlockShapeValue.T: Color.MAGENTA;
            default: throw 'Invalid shape type:' + shape;
        }};
    }

}
