package haxeris.core.factory;

import haxeris.core.factory.base.ShapedBlockFactoryBase;
import haxeris.core.enums.BlockShape;

typedef ShapedBlockValue = {shape:BlockShape};

class ShapedBlockFactory extends ShapedBlockFactoryBase<ShapedBlockValue> {

    public function new() {
        super();
    }

    override public function getValueForShape(shape:BlockShape){
        return {shape:shape};
    }

}
