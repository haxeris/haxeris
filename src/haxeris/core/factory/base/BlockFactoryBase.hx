package haxeris.core.factory.base;

class BlockFactoryBase<T> {

    private function new() {
    }

    public function createBlock():Block<T>{
        throw 'Abstract method must be overridden!';
    }
}
