package haxeris.core.factory.base;

import haxeris.core.enums.BlockShape;

class ShapedBlockFactoryBase<T> extends BlockFactoryBase<T> {

    private static var Z(default, never) = [p(0, 0), p(1, 0), p(0, -1), p(-1, -1)];
    private static var O(default, never) = [p(0, 0), p(1, 0), p(1, 1), p(0, 1)];
    private static var I(default, never) = [p(0, -2), p(0, -1), p(0, 0), p(0, 1)];
    private static var L(default, never) = [p(0, 0), p(1, 0), p(0, -1), p(0, -2)];
    private static var T(default, never) = [p(0, 0), p(-1, 0), p(0, 1), p(1, 0)];

    private static var ALL_SHAPES = Type.allEnums(BlockShapeValue);

    public function new() {
        super();
    }

    override public function createBlock() {
        var randomShape = ALL_SHAPES[Std.int(Math.random() * ALL_SHAPES.length)];
        return createShapedBlock(randomShape);
    }

    public function createShapedBlock(shape:BlockShape):Block<T> {
        var value = getValueForShape(shape);
        return switch shape{
            case BlockShapeValue.Z: createShapeBlock(Z, value);
            case BlockShapeValue.O: createShapeBlock(O, value);
            case BlockShapeValue.I: createShapeBlock(I, value);
            case BlockShapeValue.L: createShapeBlock(L, value);
            case BlockShapeValue.T: createShapeBlock(T, value);
            default: throw 'Invalid shape type:' + shape;
        }
    }

    public function getValueForShape(shape:BlockShape):T{
        throw 'Abstract method must be overridden!';
    }

    private inline function createShapeBlock<T>(positions:Array<{x:Int, y:Int}>, value:T):Block<T> {
        return Block.fromArray(positions.map(position -> return new BlockCell(position.x, position.y, value)));
    }

    private static inline function p(x:Int, y:Int) {
        return {x:x, y:y};
    }
}
