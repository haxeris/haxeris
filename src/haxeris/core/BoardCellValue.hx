package haxeris.core;

import haxe.ds.Option;

abstract BoardCellValue<T>(Option<{val:T, isActive:Bool}>){
    public static var EMPTY(default, never) = new BoardCellValue();

    public inline function new(val:T = null, isActive:Bool = false) {
        this = val == null ? None : Some({val:val, isActive:isActive});
    }

    public inline function isEmpty(){
        return Type.enumEq(this, None);
    }

    @:to
    public inline function getValue():T{
        return switch this{
            case None: null;
            case Some(v): v.val;
        }
    }

    public inline function isActive():Bool{
        return switch this{
            case None: false;
            case Some(v): v.isActive;
        }
    }

    @:from
    static public function fromValue<T>(v:T)
    	    return new BoardCellValue<T>(v);

    @:from
    static public function fromObject<T>(v:{val:T, isActive:Bool})
        return new BoardCellValue<T>(v.val, v.isActive);
}