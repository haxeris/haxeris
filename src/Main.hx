package ;

import haxeris.controller.GameController;
import haxeris.core.factory.ShapedBlockFactory;
import js.Browser.document;
import coconut.Ui.hxx;
import haxeris.data.Game;
import haxeris.ui.GameView;

class Main{

  public static inline var BOARD_WIDTH:Int = 10;
  public static inline var BOARD_HEIGHT:Int = 25;

  public static var DefaultBlockFactory(default, never):ShapedBlockFactory = new ShapedBlockFactory();

  static function main() {

    var game = Game.create(BOARD_WIDTH, BOARD_HEIGHT, DefaultBlockFactory.createBlock);
    game.newGame();

    var gameController = new GameController(game);

    coconut.ui.Renderer.mount(
      cast document.body.appendChild(document.createDivElement()),
      hxx('<GameView game=${game}/>')
    );
  }

}