package haxeris;

import haxeris.core.Board;
import haxeris.core.enums.BlockShape;
import haxeris.core.factory.ShapedBlockFactory;
import haxeris.data.Game;

import massive.munit.Assert;

class GameTest
{
    private static var DEFAULT_QUE(default, never) = [
        BlockShapeValue.I,
        BlockShapeValue.I,
        BlockShapeValue.I,
        BlockShapeValue.I,
    ];

    private static inline var WIDTH:Int = 5;
    private static inline var HEIGHT:Int = 6;

    var testQue:TestBlockQueue;
    var game:Game;

    public function new()
    {
    }

    @BeforeClass
    public function beforeClass():Void
    {
    }

    @AfterClass
    public function afterClass():Void
    {
    }

    @Before
    public function setup():Void
    {
        testQue = new TestBlockQueue();
        game = Game.create(WIDTH, HEIGHT, testQue.getBlock);
    }

    @After
    public function tearDown():Void
    {
    }

    @Test
    public function test_gameStates()
    {
        Assert.areEqual(game.width, WIDTH);
        Assert.areEqual(game.height, HEIGHT);

        testQue.setQue(DEFAULT_QUE);
        game.newGame();
        Assert.areEqual(boardToString(game.gameBoard), [
            '.|.|x|.|.',
            '.|.|x|.|.',
            '.|.|x|.|.',
            '.|.|x|.|.',
            '.|.|.|.|.',
            '.|.|.|.|.'
        ].join('\n'));

        Assert.isTrue(Type.enumEq(game.gameState, GameState.PAUSED));
        game.run();
        Assert.isTrue(Type.enumEq(game.gameState, GameState.RUNING));
        game.gameOver();
        Assert.isTrue(Type.enumEq(game.gameState, GameState.GAME_OVER));
    }

    @Test
    public function test_moveDown()
    {
        testQue.setQue([BlockShapeValue.T]);
        game.newGame();
        game.tryMovePlayerBlock(DOWN);

        Assert.areEqual(boardToString(game.gameBoard), [
            '.|.|.|.|.',
            '.|x|x|x|.',
            '.|.|x|.|.',
            '.|.|.|.|.',
            '.|.|.|.|.',
            '.|.|.|.|.'
        ].join('\n'));
    }

    @Test
    public function test_cannotRotate()
    {
        testQue.setQue([BlockShapeValue.T]);
        game.newGame();


        game.tryRotatePlayerBlock(LEFT);
        Assert.areEqual(boardToString(game.gameBoard), [
            '.|x|x|x|.',
            '.|.|x|.|.',
            '.|.|.|.|.',
            '.|.|.|.|.',
            '.|.|.|.|.',
            '.|.|.|.|.'
        ].join('\n'));
    }

    @Test
    public function test_rotate()
    {
        testQue.setQue([BlockShapeValue.T]);
        game.newGame();

        Assert.areEqual(boardToString(game.gameBoard), [
            '.|x|x|x|.',
            '.|.|x|.|.',
            '.|.|.|.|.',
            '.|.|.|.|.',
            '.|.|.|.|.',
            '.|.|.|.|.'
        ].join('\n'));


        game.tryMovePlayerBlock(DOWN);
        game.tryMovePlayerBlock(DOWN);
        game.tryRotatePlayerBlock(LEFT);
        Assert.areEqual(boardToString(game.gameBoard), [
            '.|.|.|.|.',
            '.|.|x|.|.',
            '.|.|x|x|.',
            '.|.|x|.|.',
            '.|.|.|.|.',
            '.|.|.|.|.'
        ].join('\n'));
    }


    @Test
    public function test_GameOver()
    {
        testQue.setQue([
            BlockShapeValue.O,
            BlockShapeValue.Z,
            BlockShapeValue.T,
        ]);
        game.newGame();

        for(i in 0...8){
            game.step();
            Assert.isTrue(Type.enumEq(game.gameState, GameState.PAUSED));
        }

        game.step();
        Assert.isTrue(Type.enumEq(game.gameState, GameState.GAME_OVER));

    }

    private static function boardToString(board:Board<ShapedBlockValue>):String{
        return board.toArrayOfBoardCell().map(row->row.map(val->val.value.getValue() == null ? '.' : 'x').join('|')).join('\n');
    }
}

private class TestBlockQueue{
    private var blockFactory:ShapedBlockFactory;
    private var queue:Array<BlockShape>;

    private var head:Int = 0;

    public function new(){
        blockFactory = new ShapedBlockFactory();
        this.queue = [];
    }

    public function setQue(queue:Array<BlockShape>){
        this.queue = queue;
        head = 0;
    }

    public function getBlock(){
        if(head == queue.length) head = 0;
        return blockFactory.createShapedBlock(queue[head++]);
    }
}
