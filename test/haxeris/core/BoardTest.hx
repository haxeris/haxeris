package haxeris.core;

import haxeris.core.enums.BlockShape;
import massive.munit.Assert;
import haxeris.core.Board; 

class BoardTest
{
    private static inline var WIDTH:Int = 3;
    private static inline var HEIGHT:Int = 5;

    private static var DEFAULT_BLOCK(default, never) = new BoardCellValue(BlockShapeValue.I);

    var board:Board<BlockShape>;

    public function new()
    {
    }

    @BeforeClass
    public function beforeClass():Void
    {
    }

    @AfterClass
    public function afterClass():Void
    {
    }

    @Before
    public function setup():Void
    {
        board = new Board(WIDTH, HEIGHT);
    }

    @After
    public function tearDown():Void
    {
    }

    @Test
    public function test_get_Rows()
    {
        Assert.areEqual(board.rows, HEIGHT);
    }

    @Test
    public function test_get_Columns()
    {
        Assert.areEqual(board.columns, WIDTH);
    }

    @Test
    public function test_getNumOfCells()
    {
        Assert.areEqual(board.numOfCells, WIDTH * HEIGHT);
    }

    @Test
    public function test_isInside()
    {
        Assert.isTrue(board.isInside(0,0));
        Assert.isTrue(board.isInside(WIDTH-1,HEIGHT-1));

        Assert.isFalse(board.isInside(-1,0));
        Assert.isFalse(board.isInside(-1,-1));
        Assert.isFalse(board.isInside(0,-1));

        Assert.isFalse(board.isInside(WIDTH,0));
        Assert.isFalse(board.isInside(WIDTH,HEIGHT));
        Assert.isFalse(board.isInside(0,HEIGHT));
    }

    @Test
    public function test_getAt()
    {
        for(x in 0...WIDTH)
            for(y in 0...HEIGHT){
                var value = board.getAt(x, y).value;
                Assert.isTrue(value.isEmpty());
            }
    }

    @Test
    public function test_isRowFull()
    {
        for(y in 0...HEIGHT)
            Assert.isFalse(board.isRowFull(y));
    }

    @Test
    public function test_areEmpty()
    {
        var coords = [for(x in 0...WIDTH) for(y in 0...HEIGHT) {x:x, y:y}];

        Assert.isTrue(board.areEmpty(coords));

        var coords2 = coords.copy();
        coords2.push({x:WIDTH, y:0});
        Assert.isFalse(board.areEmpty(coords2));

        board.setAt(DEFAULT_BLOCK, 1, 1);

        Assert.isFalse(board.areEmpty(coords));
    }

    @Test
    public function test_setAt()
    {
        board.forEach((cell, x, y)->cell.set(DEFAULT_BLOCK));

        for(x in 0...WIDTH)
            for(y in 0...HEIGHT){
                var value = board.getAt(x, y).value;
                Assert.areEqual(value, DEFAULT_BLOCK);
            }
    }

    @Test
    public function test_clearRow()
    {
        board.forEach((cell, x, y)->cell.set(DEFAULT_BLOCK));

        for(y in 0...HEIGHT){
            Assert.isTrue(board.isRowFull(y));
            board.clearRow(y);
            Assert.isFalse(board.isRowFull(y));
        }
    }

    @Test
    public function test_removeRow()
    {
        var removeRow = (row:Int)->{
            board.clear();

            if(row > 0){
                board.setAt(DEFAULT_BLOCK, 0, row);
                board.setAt(DEFAULT_BLOCK, 1, row);
                board.setAt(DEFAULT_BLOCK, 2, row);

                board.setAt(DEFAULT_BLOCK, 1, row-1);
            }else{
                board.setAt(DEFAULT_BLOCK, 1, row);
            }

            board.removeRow(row);

            Assert.isTrue(board.isEmpty(0, row));
            if(row > 0){
                Assert.isFalse(board.isEmpty(1, row));
            }else{
                Assert.isTrue(board.isEmpty(1, row));
            }
            Assert.isTrue(board.isEmpty(2, row));
        }

        for(i in 0...HEIGHT)
            removeRow(i);

    }

    @Test
    public function test_clear()
    {
        board.forEach((cell, x, y)->cell.set(DEFAULT_BLOCK));

        for(y in 0...HEIGHT)
            Assert.isTrue(board.isRowFull(y));


        board.clear();

        for(y in 0...HEIGHT)
            Assert.isFalse(board.isRowFull(y));

    }

    private static function boardToString(board:Board<BlockShape>):String{
        return board.toArrayOfBoardCell().map(row->row.map(val->val.value.getValue() == null ? '.' : 'x').join('|')).join('\n');
    }
}
