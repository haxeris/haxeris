package haxeris.core;

import haxeris.core.factory.ShapedBlockFactory;
import massive.munit.Assert;
import haxeris.core.enums.BlockShape;

class BlockFactoryTest
{
    var factory:ShapedBlockFactory;

    public function new()
    {
    }

    @BeforeClass
    public function beforeClass():Void
    {
    }

    @AfterClass
    public function afterClass():Void
    {
    }

    @Before
    public function setup():Void
    {
        factory = new ShapedBlockFactory();
    }

    @After
    public function tearDown():Void
    {
    }


    @Test
    public function test_createZ()
    {
        var shapeStr = [
            'x|x|.',
            '.|x|x'
        ].join('\n');

        var block = factory.createShapedBlock(BlockShapeValue.Z);

        Assert.areEqual(block.toString(), shapeStr);
    }

    @Test
    public function test_createO()
    {
        var shapeStr = [
            'x|x',
            'x|x'
        ].join('\n');

        var block = factory.createShapedBlock(BlockShapeValue.O);

        Assert.areEqual(block.toString(), shapeStr);
    }


    @Test
    public function test_createI()
    {
        var shapeStr = [
            'x',
            'x',
            'x',
            'x'
        ].join('\n');

        var block = factory.createShapedBlock(BlockShapeValue.I);

        var bounds = block.calculateBouns();
        Assert.areEqual(block.toString(), shapeStr);
    }

    @Test
    public function test_createL()
    {
        var shapeStr = [
            'x|.',
            'x|.',
            'x|x'
        ].join('\n');

        var block = factory.createShapedBlock(BlockShapeValue.L);

        Assert.areEqual(block.toString(), shapeStr);
    }

    @Test
    public function test_createT()
    {
        var shapeStr = [
            'x|x|x',
            '.|x|.'
        ].join('\n');

        var block = factory.createShapedBlock(BlockShapeValue.T);

        Assert.areEqual(block.toString(), shapeStr);
    }
}
