import massive.munit.TestSuite;

import haxeris.core.BoardTest;
import haxeris.core.BlockFactoryTest;
import haxeris.GameTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(haxeris.core.BoardTest);
		add(haxeris.core.BlockFactoryTest);
		add(haxeris.GameTest);
	}
}
