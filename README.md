# HAXERIS

It's like just a simple tetris game, written in haxe 

## Setup and Build

1. **Install**: run `npm install`
2. **Build**: run `npm run build`
3. **Run tests**: run `npm run test`
4. **Play**: open `bin/index.html` in a browser